#ifndef	MBLUR_H
#define MBLUR_H
#include "common.h"

#ifndef	USE_MBLUR
	half3 mblur (float2 UV, half3 pos, half3 c_original) { return c_original; }
#else

uniform half4x4	m_current;
uniform half4x4	m_previous;
uniform half2 	m_blur;

float3 	mblur	(float2 UV, float3 pos, float3 c_original)	
{
	pos.z   			+= (saturate(0.001 - pos.z)*10000.h);
	float4 	pos4		= float4 (pos,0.01h);
	float4 	p_current	= mul (m_current, pos4);
	float4 	p_previous 	= mul (m_previous, pos4);
	float2 	p_velocity 	= m_blur * ( (p_current.xy/p_current.w)-(p_previous.xy/p_previous.w) );
			p_velocity	= clamp	(p_velocity,-MBLUR_CLAMP,+MBLUR_CLAMP);

	#ifdef MBLUR_WPN_DISABLE
		if (pos.z < 2.5) // disable for hud
			p_velocity = 0.0;
	#endif
		
	float3	blurred	= float3(0.f, 0.f, 0.f);

	for (int i = 1; i <= MBLUR_SAMPLES; i++)
	{
		blurred += tex2D(s_image, p_velocity * half(i) + UV).rgb;
	}
	
	return blurred/MBLUR_SAMPLES;
}
#endif
#endif
