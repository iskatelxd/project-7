#ifndef SSFXS_H
#define SSFXS_H

#include "common.h"

////////////////////////////////////////////////////////////////////////////////////
/// GLOBALS
////////////////////////////////////////////////////////////////////////////////////

static const float3x3 RGB = float3x3
(
2.67147117265996,-1.26723605786241,-0.410995602172227,
-1.02510702934664,1.98409116241089,0.0439502493584124,
0.0610009456429445,-0.223670750812863,1.15902104167061
);

static const float3x3 XYZ = float3x3
(
0.500303383543316,0.338097573222739,0.164589779545857,
0.257968894274758,0.676195259144706,0.0658358459823868,
0.0234517888692628,0.1126992737203,0.866839673124201
);

////////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////////

/*
  Technicolor2 v1.0 by Prod80
*/

float3 TechnicolorPass(float3 colorInput)
{
	float3 Color_Strength = float3(Technicolor2_Red_Strength,Technicolor2_Green_Strength,Technicolor2_Blue_Strength);
	float3 source = saturate(colorInput.rgb);
	float3 temp = 1.0 - source;
	float3 target = temp.grg;
	float3 target2 = temp.bbr;
	float3 temp2 = source.rgb * target.rgb;
	temp2.rgb *= target2.rgb;

	temp.rgb = temp2.rgb * Color_Strength;
	temp2.rgb *= Technicolor2_Brightness;

	target.rgb = temp.grg;
	target2.rgb = temp.bbr;

	temp.rgb = source.rgb - target.rgb;
	temp.rgb += temp2.rgb;
	temp2.rgb = temp.rgb - target2.rgb;

	colorInput.rgb = lerp(source.rgb, temp2.rgb, Technicolor2_Strength);

	colorInput.rgb = lerp(dot(colorInput.rgb, 0.333), colorInput.rgb, Technicolor2_Saturation); 
	
	return colorInput.rgb;
}

/*
  DPX/Cineon v1.1 shader by Loadus, CeeJay.dk
*/

float3 DPXPass(float3 InputColor)
{
	float DPXContrast = 0.1;
	float DPXGamma = 1.0;
	float RedCurve = Red;
	float GreenCurve = Green;
	float BlueCurve = Blue;
	float3 RGB_Curve = float3(Red,Green,Blue);
	float3 RGB_C = float3(RedC,GreenC,BlueC);
	float3 B = InputColor.rgb;
	B = pow(abs(B), 1.0/DPXGamma);
	B = B * (1.0 - DPXContrast) + (0.5 * DPXContrast);
    float3 Btemp = (1.0 / (1.0 + exp(RGB_Curve / 2.0)));	  
	B = ((1.0 / (1.0 + exp(-RGB_Curve * (B - RGB_C)))) / (-2.0 * Btemp + 1.0)) + (-Btemp / (-2.0 * Btemp + 1.0));
	float value = max(max(B.r, B.g), B.b);
	float3 color = B / value;
	color = pow(abs(color), 1.0/ColorGamma);
	float3 c0 = color * value;
	c0 = mul(XYZ, c0);
	float luma = dot(c0, float3(0.30, 0.59, 0.11));
	c0 = (1.0 - DPXSaturation) * luma + DPXSaturation * c0;
	c0 = mul(RGB, c0);
	InputColor.rgb = lerp(InputColor.rgb, c0, Blend);

	return InputColor;
}

/*
  ReinhardLinear Tonemap by Marty McFly
*/

float3 ReinhardLinearPass( float3 colorInput )
{
	float3 x = colorInput.rgb;
    	const float W = ReinhardLinearWhitepoint;
    	const float L = ReinhardLinearPoint;
    	const float C = ReinhardLinearSlope;
    	const float K = (1 - L * C) / C;
    	float3 reinhard = L * C + (1 - L * C) * (1 + K * (x - L) / ((W - L) * (W - L))) * (x - L) / (x - L + K);
    	colorInput.rgb = (x > L) ? reinhard : C * x;

	return colorInput;
}

#endif