#ifndef SLOAD_H
#define SLOAD_H

#include "common.h"

//////////////////////////////////////////////////////////////////////////////////////////
// Bumped surface loader                //
//////////////////////////////////////////////////////////////////////////////////////////
struct	surface_bumped
{
	float4	base;
	float3	normal;
	float	gloss;
	float	height;

};

float4 tbase( float2 tc )
{
	return	tex2D( s_base, tc);
}

#if defined(ALLOW_STEEPPARALLAX) && defined(USE_STEEPPARALLAX)

#define POM_START_FADE float(12.0f)
#define POM_STOP_FADE float(16.0f)

void UpdateTC( inout p_bumped I)
{
	if (I.position.z < POM_STOP_FADE)
	{

#define POM_MAX_SAMPLES 45
#define POM_MIN_SAMPLES 10
#define POM_OFFSET float(-0.02f)

		float3	 eye = mul (float3x3(I.M1.x, I.M2.x, I.M3.x,
									 I.M1.y, I.M2.y, I.M3.y,
									 I.M1.z, I.M2.z, I.M3.z), -I.position.xyz);

		eye = normalize(eye);
		
		// Calculate number of steps
		float nNumSteps = lerp( POM_MAX_SAMPLES, POM_MIN_SAMPLES, eye.z );

		float	fStepSize			= 1.0 / nNumSteps;
		float2	vDelta				= eye.xy * POM_OFFSET * 1.2;
		float2	vTexOffsetPerStep	= fStepSize * vDelta;

		// Prepare start data for cycle
		float2	vTexCurrentOffset	= I.tcdh;
		float	fCurrHeight			= 0.0;
		float	fCurrentBound		= 1.0;
		float	fPrevHeight			= 0.0;	
		float   fPrevBound			= 1.0;

		for( int i=0; i<nNumSteps; ++i )
		{
			if (fCurrHeight < fCurrentBound)
			{	
				fPrevHeight = fCurrHeight;
				fPrevBound  = fCurrentBound;
				
				vTexCurrentOffset += vTexOffsetPerStep;		
			fCurrHeight = tex2Dlod( s_bumpX, float4(vTexCurrentOffset.xy,0,0) ).a; 
				fCurrentBound -= fStepSize;
			}
//			else
//			{
//				break;
//			}
		}

		//	Smooth tc position between current and previouse step
		float	fDelta2 = (fPrevBound - fPrevHeight);
		float	fDelta1 = (fCurrentBound - fCurrHeight);
		float	fParallaxAmount = (fCurrentBound * fDelta2 - fPrevBound * fDelta1 ) / ( fDelta2 - fDelta1 );
		float	fParallaxFade 	= smoothstep(POM_STOP_FADE, POM_START_FADE, I.position.z);
		float2	vParallaxOffset = vDelta * ((1- fParallaxAmount )*fParallaxFade);
		float2	vTexCoord = I.tcdh + vParallaxOffset;
	
		//	Output the result
		I.tcdh = vTexCoord;

#if defined(USE_TDETAIL) && defined(USE_STEEPPARALLAX)
		I.tcdbump = vTexCoord * dt_params;
#endif
	}

}

#elif	defined(USE_PARALLAX) || defined(USE_STEEPPARALLAX)

void UpdateTC( inout p_bumped I)
{
	float3	 eye = mul (float3x3(I.M1.x, I.M2.x, I.M3.x,
								 I.M1.y, I.M2.y, I.M3.y,
								 I.M1.z, I.M2.z, I.M3.z), -I.position.xyz);
	
	float	height	= tex2D(s_bumpX, I.tcdh).w;
			height	= height*(parallax.x) + (parallax.y);
	float2	new_tc  = I.tcdh + height * normalize(eye);

	//Output the result
	I.tcdh.xy = new_tc;
}

#else	//USE_PARALLAX

void UpdateTC( inout p_bumped I)
{
	;
}

#endif	//USE_PARALLAX

surface_bumped sload_i( p_bumped I)
{
	surface_bumped	S;

	UpdateTC(I);	//All kinds of parallax are applied here.

	float4 	Nu	= tex2D( s_bump, I.tcdh);	//IN: normal.gloss
	float4 	NuE	= tex2D( s_bumpX, I.tcdh);	//IN: normal_error.height

	S.base		= tbase(I.tcdh);				//IN: rgb.a
	S.normal	= Nu.wzy + (NuE.xyz - 1.0h); // + (NuE.xyz - 1.0h);	//(Nu.wzyx - .5h) + (E-.5)
	S.gloss		= Nu.x*Nu.x;
	S.height	= NuE.w;

#ifdef USE_TDETAIL
#ifdef USE_TDETAIL_BUMP
	half4 NDetail		= tex2D(s_detailBump, I.tcdbump);
	half4 NDetailX		= tex2D(s_detailBumpX, I.tcdbump);
	S.gloss				= S.gloss * NDetail.x * 2;
//    S.gloss				+= (NDetail.x)/5; //ONETWO
	S.normal			+= NDetail.wzy + NDetailX.xyz - 1.0h;
	half4 detail		= tex2D( s_detail, I.tcdbump);
	S.base.rgb			= S.base.rgb * detail.rgb * 2;
#else		//USE_TDETAIL_BUMP
	half4 detail		= tex2D(s_detail, I.tcdbump);
	S.base.rgb			= S.base.rgb * detail.rgb * 2;
	S.gloss				= S.gloss * detail.w * 2;
//    S.gloss				+= (detail.x)/5; //ONETWO
#endif		//USE_TDETAIL_BUMP
#endif
	return S;
}

surface_bumped sload ( p_bumped I)
{
        surface_bumped      S   = sload_i	(I);
		S.normal.z			*=	0.5;
        return              S;
}

#endif